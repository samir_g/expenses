import React, { useState } from "react";
import "./ExpenseForm.css";

function ExpenseForm(props) {
  const [enterDate, setEnterDate] = useState("");
  const dateChanger = (event) => {
    setEnterDate(event.target.value);
  };

  const [enterAmount, setEnterAmount] = useState("");
  const amountChanger = (event) => {
    setEnterAmount(event.target.value);
  };

  const [enterTitle, setEnterTitle] = useState("");
  const titleChanger = (event) => {
    setEnterTitle(event.target.value);
  };
  const sumbitHandler = (event) => {
    event.preventDefault();
    const expenseDate = {
      title: enterTitle,
      amount: +enterAmount,
      date: new Date(enterDate),
    };
    props.onSaveExpenseData(expenseDate);
    setEnterAmount("");
    setEnterDate("");
    setEnterTitle("");
  };

  return (
    <form onSubmit={sumbitHandler}>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Title</label>
          <input type="text" onChange={titleChanger} value={enterTitle} />
        </div>
        <div className="new-expense__control">
          <label>Amount</label>
          <input
            type="number"
            min="0.01"
            step="0.01"
            onChange={amountChanger}
            value={enterAmount}
          />
        </div>
        <div className="new-expense__control">
          <label>Date</label>
          <input
            type="date"
            min="2019-01-01"
            max="2022-12-31"
            onChange={dateChanger}
            value={enterDate}
          />
        </div>
        <div className="new-expense__actions">
          <button type="button" onClick={props.onCancel}>
            Cancel
          </button>
          <button type="submit">Add expense</button>
        </div>
      </div>
    </form>
  );
}

export default ExpenseForm;
